.. title: Welcome to Invenius
.. slug: welcome-to-invenius
.. date: 2018-04-16 23:00:00 UTC-03:00
.. tags: invenius, java, c++, nikola, blog
.. author: Frank Finner
.. link: https://invenius.gitlab.io/nikola/
.. description:
.. category: nikola

.. figure:: https://farm1.staticflickr.com/138/352972944_4f9d568680.jpg
   :target: https://farm1.staticflickr.com/138/352972944_4f9d568680_z.jpg?zz=1
   :class: thumbnail
   :alt: Nikola Tesla Corner by nicwest, on Flickr

If you can see this in a web browser, it means you managed to install NikolaInvenius,
and build a site using it. Congratulations!

Next steps:

* :doc:`Read the manual <handbook>`
* `Visit the Nikola website to learn more <https://getnikola.com>`__
* `See a demo photo gallery <link://gallery/demo>`__
* :doc:`See a demo listing <listings-demo>`
* :doc:`See a demo slideshow <slides-demo>`
* :doc:`See a demo of a longer text <dr-nikolas-vendetta>`

Send feedback to info@invenius.org!
